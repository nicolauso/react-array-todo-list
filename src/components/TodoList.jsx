import PropTypes from "prop-types";
import TodoItem from "./TodoItem";

function TodoList(props) {
  const { todos } = props;

  // Création pour chaque élément(todo.id) du tableau, création d'un nouveau composant TodoItem avec comme props todo.id, todo.title, todo.completed.
  // Utilisation de la props todos[] qui provient de App.jsx

  const listItems = todos.map(todo => (
    <TodoItem
      key={todo.id}
      title={todo.title}
      completed={todo.completed}
    />
  ));

  return (
    <div>
      <h1>My todo list ({todos.length} items):</h1>
      <ul>{listItems}</ul>
    </div>
  );
}

TodoList.propTypes = {
  todos: PropTypes.array.isRequired,
};

export default TodoList;
